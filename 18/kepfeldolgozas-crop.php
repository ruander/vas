<?php
$img = "kaja2.jpg";

$info = getimagesize($img);
//var_dump($info);
$validTypes = [//ilyen tipusokat engedünk
    'jpeg'=> 'image/jpeg',
    'png' => 'image/png',
    'gif' => 'image/gif'
];
$type = array_search($info['mime'],$validTypes);//ez a kép tipusa
$origWidth = $info[0];
$origHeight = $info[1];
$ratio = $origWidth/$origHeight;//képarány (<1 álló ,0 négyzet , >1 fekvő)

//feladat: 3 méret elkészítése
//640x640 helyre méretarányos kicsinyítés - kepfeldolgozas.php-ban
//150x150 thumbnail elkészítése - crop
$targetWidth=150;
$targetHeight=150;
if($ratio>1){//fekvő
    //uj width,height
    $width = round($targetHeight * $ratio); //lelógó képet kapunk
    $height = $targetHeight;
    $dX = 0;
    $dY = 0;
    $sX = ($width-$targetWidth)/2;
    $sY = 0;
}else{//álló vagy négyzet
    //uj width,height
    $height = round($targetWidth/$ratio); //lelógó képet kapunk
    $width = $targetWidth;
    $dX = 0;
    $dY = 0;
    $sX = 0;
    $sY = ($height-$targetHeight)/2;
}
$canvas = imagecreatetruecolor($targetWidth,$targetHeight);//vászon az uj képnek
//tagfüggvény dinamikus névvel
if($type) {
    $func = 'imagecreatefrom' . $type;
    $img_p = $func($img);//eredeti kép memóriába
}else{
    die('Gebasz');
}

imagecopyresampled($canvas, $img_p, $dX, $dY , $sX, $sY, $width , $height, $origWidth, $origHeight);//képművelet egy lépésben a memóriában
//header("content-type:image/".$type);//böngészőnek azt mondjuk a fileról h kép

imagefilter($canvas, IMG_FILTER_BRIGHTNESS, 20);
/*imagefilter($canvas, IMG_FILTER_NEGATE);
imagefilter($canvas, IMG_FILTER_NEGATE);*/
//imagefilter($canvas, IMG_FILTER_COLORIZE, 0, 255, 0);

//tagfüggvény dinamikus névvel
$showFunc = 'image'.$type;
imagejpeg($canvas,'valami.jpg');//kitesszük a képet a böngészőbe

//320x320 helyre méretarányos kicsinyítés képszűrő alkalmazással - blur
//memória felszabadítása!!!!!!
imagedestroy($canvas);
imagedestroy($img_p);
