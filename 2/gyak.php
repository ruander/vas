<?php
//pure PHP file
echo 'teszt<br>';
echo 256*256*256*256;//ennyi max ip cím van
echo '<br>';
echo pow(256,4);
print '<br>';
//írjuk ki egy kockadobás eredményét
echo rand(1,6);
echo '<br>';
//írjuk ki még egy kockadobás eredményét
echo rand(1,6);
//írjuk ki a kettő összegét
//előszőr el kell tárolni két változóban, hogy később fel tudjuk használni őket
$dobas_1 = rand(1,6);//dobás 1 eltárolása
$dobas_2 = rand(1,6);// operátorok: $ ->változó ; = -> értékadás
echo '<br>dobás 1: ';
echo $dobas_1;
echo '<br>dobás 2: '.$dobas_2;//operátor: . -> konkatenálás (összefűzés stringként)
echo '<br>Összegük: '.($dobas_1 + $dobas_2);

//változótípusok
$egesz_szam = 13;//integer (int)
$szoveg = "bármilyen karakterlánc";// string
$logikai = true;//boolean vagy bool
$lebegopontos_szam = 7.2;//floating point number (float)
echo '<pre>';
var_dump($egesz_szam,$szoveg,$logikai,$lebegopontos_szam);
echo '</pre>';
//változok étékei és tipusai, csak fejlesztés közben
//dobjunk egy kockával és mondjuk meg a dobás értékét és párosságát pl:a dobás értéke: 4, ami páratlan
$dobas = rand(1,6);
echo 'A dobás értéke: '.$dobas.', ami ';
//párosság megállapításához elágazást használunk
/*
if(condition (true vagy false)){
	true
}else{
	false
}
*/
if($dobas%2 == 0){//operátorok: % -> maradékos osztás; == ->érték egyezés vizsgálat; === -> érték és tipus egyezés vizsgálat
	echo 'páros';
}else{
	echo 'páratlan';
}
///minta RolePlayingGame karakter értékeinek generálására
/*
Nameless One
Erő:16
Ügyesség:17
Intelligencia:8
Bölcsesség:13
Gyorsaság:15
------------------
*/
////ciklus
/*
for(ciklusváltozó kezdeti értéke;ciklusváltozó vizsgálata;ciklusváltozó léptetése ){
	ciklusmag
}
*/
$dobasok = 0;//ide gyűjtjük a dobások összegét
for($i=1;$i<=3;$i=$i+1){
	$dobasok = $dobasok + rand(1,6);
}
echo '<br>Erő: '.$dobasok;
//ugyanez ügyességre
$dobasok = 0;//ide gyűjtjük a dobások összegét
for($i=1;$i<=3;$i=$i+1){
	$dobasok = $dobasok + rand(1,6);
}
echo '<br>Ügyesség: '.$dobasok;

//saját eljárás 'hívása'
echo '<br>Intelligencia:'.ertekGeneralas();
echo '<br>Bölcsesség:'.ertekGeneralas();
echo '<br>Gyorsaság:'.ertekGeneralas();


//újrahasznosítható kódrészlet egy tulajdonság érték generálásra (3*(1-6))
/*
function eljarasneve(parameter1,parameter2){
		eljárás
}
*/
function ertekGeneralas(){
	$dobasok = 0;//ide gyűjtjük a dobások összegét
	//ciklus 3 dobás összegzésére
	for($i=1;$i<=3;$i=$i+1){
		$dobasok = $dobasok + rand(1,6);
	}
	return $dobasok;//visszatérés az összeggel
}