<?php
//halmazok - array (tömb)
/*
kulcs és érték (index - value vagy (k)ey - (v)alue
*/
$tomb = [1,13,56];
echo '<pre>';
var_dump($tomb);//array és az elemszám jellemzi
/*
dump eredménye:
array(3) {
  [0]=>int(1)
  [1]=>int(13)
  [2]=>int(56)
}
*/
echo $tomb;//tömb nem primitiv ezért nem is konvertálható egyszerűen stringgé kiíráshoz -> Array + notice
echo $tomb[1];//13 -> ha primitiv az adott elem akkor kiírható
$tomb[] = 'valami';//tömb bővítése automatikus indexre (3)
var_dump($tomb);
unset($tomb[3]);//tömb egy elemének eltávolítása (vagy bármilyen változóé)
var_dump($tomb);
$tomb[] = 'valami';//tömb bővítése automatikus indexre
var_dump($tomb);
$tomb[100] = '100 index';//tömb bővítése irányított indexre
var_dump($tomb);
$tomb[42] = 'teszt';
$tomb[] = 'heloworld';//101 (tömbmutató)
var_dump($tomb);
$menu = [1, 1.5, 'valami', rand(1,6)];//egy tömbben bármilyen tipusú értékek lehetnek
var_dump($menu);
$menu = [
	23 => 'elem',
	'email' => 'hgy@iworkshop.hu',
	'name' => 'Horváth György',
];//újradeklarálás
var_dump($menu);
echo $menu['name'];
//tömb (vagy objektum) bejárása
/*foreach(tömb as kulcs => érték){
		//elérhető az aktuális kulcs és érték
}
*/
foreach($menu as $k => $v){
		echo "<br>Az aktuális kulcs: $k , ehhez tartozó érték: $v";
}
var_dump(is_array('menu'));//true <- false