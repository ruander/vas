<?php
//file- és mappakezelés
$dir = 'uploads/';//ide kerülnek a feltöltött fileok
//url erőforrás kinyerése, itt küldjük hogy törölni akarunk-e
$delete = filter_input(INPUT_GET,'del');
//ha van törölni való, törlünk
if($delete != '' && is_file($dir.$delete)) {
        unlink($dir . $delete);
        //átirányítjuk a filet hogy ne virítson hogy az urlben dolgozunk (bár az a TAGeknél href ben úgy is ott lesz a praméterezés
    header('location:filefeltoltes.php');
    exit();//biztonsági exit
}

if (!is_dir($dir)) {
    mkdir($dir, 0755);
}//ha nem létezik hozzuk létre
echo '<pre>';
var_dump($_POST);//itt csak a filenév van
//var_dump($_FILES);//kell hozzá a form enctype attributuma enctype="multipart/form-data"
echo '</pre>';
echo '<pre>' . var_export($_FILES, true) . '</pre>';//php.net
//hibakezelés,feltöltés
if (!empty($_FILES)) {
    $hiba = [];
    //file hiba default üres hogy legyen mihez fűzni a több fajta hibát
    $hiba['file_to_upload'] = '';
    if (!is_uploaded_file($_FILES['file_to_upload']['tmp_name'])) {
        $hiba['file_to_upload'] .= '<span class="error">hiba a feltöltés közben!</span>';
    }
    //max 2MB beállítása után ellenőrizni kell hogy belefér-e? HF
    $sizeLimit = 2 * 1024 * 1024;//byte
    if ($_FILES['file_to_upload']['size'] > $sizeLimit) {
        $hiba['file_to_upload'] .= '<span class="error">Fileméret túl nagy (max 2MB) !</span>';
    }
    //típus korlát txt,doc,docx,pdf,rtf -> HF
    $availableFileTypes = ['txt', 'doc', 'docx', 'pdf', 'rtf'];

    //file végződés kinyerése tömb segítségével
    $tempArray = explode('.', $_FILES['file_to_upload']['name']);
    //megfordítjuk a tömböt, így a 0. elemre van szükségünk
    $forditottTomb = array_reverse($tempArray);
    $ext = $forditottTomb[0];//ez a file kiterjesztése . nélkül
    //filenévről lekapjuk a kiterjesztést a későbbi ékezettelenítéshez
    $fileName = rtrim($_FILES['file_to_upload']['name'], '.' . $ext);//php.net
    if (!in_array($ext, $availableFileTypes)) {
        $hiba['file_to_upload'] .= '<span class="error">Nem megengedett filetípus!</span>';
    }//ez a módszer 'átverhető', mert csak a file kiterjesztését vizsgálja a valódi típusát nem
    //ha nem volt hiba eltávolítjuk a default-nak létrehozott kulcsot
    if ($hiba['file_to_upload'] == '') {
        unset($hiba['file_to_upload']);
    }
    if (empty($hiba)) {
        //elvileg nincs semmi gond, mehet az 'áthelyezés'
        //de előtte átalakítjuk a filenevet ékezet és speciális karakter mentesre
        //$mit=['(á|Á)','(é|É)','(í|Í)','(ó|Ó)','(ö|Ö)','(ő|Ő)','(ú|Ú)','(ü|Ü)','(ű|Ű)',' '];//HF preg_replace hez pattern, regex kifejezéseket átnézni
        $mit = ['á', 'Á', 'é', 'É', 'í', 'Í', 'ó', 'Ó', 'ö', 'Ö', 'ő', 'Ő', 'ú', 'Ú', 'ü', 'Ü', 'ű', 'Ű', ' '];
        $mire = ['a', 'a', 'e', 'e', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'u', 'u', '-'];
        //lecseréljük
        $fileName = str_replace($mit, $mire, $fileName);
        $fileName = strtolower($fileName);//kisbetűs legyen minden
        $regexp = '([^a-z0-9])';// HF: az eredeti - _ elemek maradjanak meg

        $fileName = preg_replace($regexp, '-', $fileName) . '.' . $ext;//ékezetmentes filenév + kiterjesztés

        move_uploaded_file($_FILES['file_to_upload']['tmp_name'], $dir . $fileName);//file áthelyezése a kívánt mappába
    }
}

?><!doctype html>
<html>
<head>
    <title>File- és mappakezelés</title>
    <meta charset="utf-8">
</head>
<body>
<form method="post" enctype="multipart/form-data">
    <?php
    //ha van hiba kiírjuk
    if (isset($hiba['file_to_upload'])) {
        echo $hiba['file_to_upload'];
    }

    ?>
    <div><label for="myFile">
            File feltöltése: <input type="file" name="file_to_upload" id="myFile">
        </label>
    </div>
    <button>Feltölt</button>
</form>
<h2>Feltöltött file(ok):</h2>
<?php
echo 'filelista';
//HF, ha nincsenek fileok, írjuk ki ...
if ($handle = opendir($dir)) {
var_dump(count($handle));
    //az összes elemét kiírjuk ciklusban, amíg találunk elemet azaz a readdir nem lesz hamis
    while (false !== ($entry = readdir($handle))) {
        // echo '<br><a href="'.$dir.$entry.'" target="_blank">'.$entry.'</a>';
        //csak " -el
        //. és .. elemek nem kellenek
        if($entry != '.' && $entry != '..') {
            echo "<br><a href=\"{$dir}{$entry}\" target=\"_blank\">$entry</a> | <a href=\"?del=$entry\" onclick='window.confirm()'>X</a>";
        }
    }
    closedir($handle);
}
?>

</body>
</html>
