<!doctype html>
<html lang="hu">
<head>
    <title>PHP programozás - gyakorlás</title>
    <meta charset="utf-8">
</head>
<body>
<h3>Készítsen egy ciklust, ami menüpontokat ír ki!</h3>
<nav>
    <ul>
        <?php
        for ($i = 1; $i <= rand(5, 8); $i++) {
            echo '<li><a href="?menu-' . $i . '">menüpont ' . $i . '</a></li>';
        }
        ?>
    </ul>
</nav>
<nav>
    <ul>
        <?php
        //menüt egy tömbből fogjuk felépíteni
        $menu = [
            1 => 'Kezdőlap',
            2 => 'Rólunk',
            3 => 'Szolgáltatások',
            4 => 'Kapcsolat',
            5 => 'Akció',
        ];
        //foreach a menu tömb bejárására
        foreach ($menu as $k => $v) {
            echo "<li><a href=\"?menu-$k\">$v</a></li>";// operátor: \ -> escape! az utána következő elemet kiveszi a végrehajtható nyelvi elemek közül
        }
        ?>
    </ul>
</nav>
</body>
</html>