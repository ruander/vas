<pre><?php
//Asszociatív tömb 2.
$data = [
    1 => [
        'name' => 'Nameless One',
        'email' => 'hgy@ruander.hu',
        'abilities' => [
            'ero' => 15,
            'ugyesseg' => 13,
            'intelligencia' => 10,
            'gyorsasag' => 14,
            'bolcsesseg' => 18,
        ]
    ],
];
var_dump($data);
foreach ($data as $id => $character) {//bejárjuk a data halmazt

    if (is_array($character)) {//ha az aktuális érték tömb bejárjuk

        foreach ($character as $infoName => $info) {

            if (is_array($info)) {//ha az aktuális érték tömb bejárjuk
                foreach ($info as $ability => $abilityValue) {
                    echo "<br>$ability: $abilityValue";//tulajdonság kiírások
                }
            } else {
                echo $info;
            }
        }
    } else {//ha nem tömb akkor primitiv, így kiírható
        echo $character;
    }
}
tombBejarKiir($data);//eljárás hívása

/**
 * Rekurzív tömb bejáró kiíró eljárás
 * @param mixed $data
 * @return void
 */
function tombBejarKiir($data){

    if(is_array($data)){//ha tömb
        foreach($data as $k => $v){//bejárjuk
            echo "<br>$k:";
            tombBejarKiir($v);//rekurzív hívás
        }
    }else{
        echo $data;//nem tömb, kiírható
    }
}