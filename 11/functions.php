<?php
//saját eljárások gyüjteménye
//beléptetés
function login($userdata = []){
    global $link,$secret_key;
    $email = $userdata['email'];
    $password = $userdata['password'].$secret_key;

    //var_dump($password);
    //beragadt bejelentkezések törlése
    mysqli_query($link,"DELETE FROM sessions WHERE sid = '".session_id()."' ");
    //felh lekérése email alapján ha statusa 1
    $qry = "SELECT id,username,password,email FROM users WHERE 
            email= '$email' and 
            `status` = 1
            LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));

    $row = mysqli_fetch_assoc($result);
    if ($row) {
        //bcryptes jelszó ellenőrzés

        if(password_verify($password,$row['password'])) {

            //felhasználó azonosítása a mf azonosítás alapján történik
            $sid = session_id();
            //mf jelszó
            $spass = md5($row['id'] . $secret_key . $sid);
            //ekkor történt:
            $stime = time();
            $_SESSION['userdata'] = $row;
            //eltároljuk az érvényes munkafolyamat adatokat
            $qry = "INSERT INTO sessions(sid,spass,stime) 
            VALUES('$sid','$spass' ,$stime)  ";
            mysqli_query($link, $qry) or die(mysqli_error($link));

            return true;
        }else{
            return false;
        }
    }
    //nem kell visszatérni, mert innentől az auth azonosít
}

//érvényes belépés ellenőrzése
function auth()
{
    global $link, $secret_key;//ezeket látni kell az eljárásnak
    $time_limit = time() - 15 * 60;//15 perc
    //lejárt munkafolyamatok takarítása
    mysqli_query($link,"DELETE FROM sessions WHERE stime < $time_limit") or die(mysqli_error($link));
    $qry = "SELECT spass FROM sessions WHERE 
            sid = '" . session_id() . "'  
            and stime > $time_limit
            LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($result);// [0] kulcson lesz az spass

    if ($row) {//találtam session id alapján rekordot
        //spass ujragenerálása
        $spass = md5($_SESSION['userdata']['id'] . $secret_key . session_id());
        if ($spass === $row[0]) {
            //stime update
            mysqli_query($link,
                "UPDATE sessions SET stime = '".time()."' 
                      WHERE sid = '".session_id()."'");
            return true;
        }
    }
    return false;
}
/**
 * eljárás a value kialakítására az input elemeknek -adatsúlyozás
 * @param string $fieldName - input mező neve
 * @param array $rowData - adatok a dbből, ha van
 * @return string - súlyozás -> post adat -> row adat -> semmi
 */
function checkValue($fieldName , $rowData = []){

    if(filter_input(INPUT_POST, $fieldName) !== null){
        return filter_input(INPUT_POST, $fieldName);
    }
    //ha van db adat térjünk vissza azzal
    if(array_key_exists($fieldName,$rowData)){
        return $rowData[$fieldName];
    }
    //ha nincs semmi akkor üres stringgel térünk vissza
    return '';
}
