<?php
//felhasználók adminisztrálását végző file (CRUD)
//erőforrások
$action = filter_input(INPUT_GET, 'act');
$t_id = filter_input(INPUT_GET, 'tid', FILTER_VALIDATE_INT);//csak egész szám lehet
$dbTable =  'users';
//van mit feldolgozni?
if (!empty($_POST)) {
    $hiba = [];
    //validálási logika
    //felh név kötelező
    $username = filter_input(INPUT_POST, 'username');
    if (!$username) {
        $hiba['username'] = '<span class="error">Kötelező kitölteni!</span>';
    }
    //email
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    if (!$email) {
        $hiba['email'] = '<span class="error">Hibás formátum!</span>';
    } else {
        //van-e már ilyen
        $qry = "SELECT id FROM $dbTable WHERE email = '$email' LIMIT 1";
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        //update miatt ha saját id-t adja vissza az nem hiba
        if (isset($row[0]) and $row[0] != $t_id) {
            $hiba['email'] = '<span class="error">Már regisztrált email!</span>';
        }
    }

    //jelszó min 6 karakter és 2x kell beírni és ha módosítás van, csak akkor ellenőrzünk ha az 1. mezőbe akár 1 karakter szerepel
    $pass = filter_input(INPUT_POST, 'password');
    $repass = filter_input(INPUT_POST, 'repassword');
    if($action == 'new' or $pass != '') {
        if (mb_strlen($pass, 'utf-8') < 6) {
            $hiba['password'] = '<span class="error">Túl rövid jelszó!</span>';
        } elseif ($pass != $repass) {
            $hiba['password'] = '<span class="error">Jelszavak nem egyeztek!</span>';
        } else {
            //$secret_key = 'S3cr3T_K3y!@';
            $pass = password_hash($pass . $secret_key, PASSWORD_BCRYPT);
        }
    }

    if (empty($hiba)) {
        //művelet
        //adatok 'tisztázása'
        //update és new leválasztása miatt lehet hogy nem lesz jelszó (update esetén) ekkor nem üríthetjük a db be sem
        $data = [
            'email' => $email,
            'password' => $pass,//!!!!
            'username' => $username,
            'status' => 1,
            'time_created' => date('Y-m-d H:i:s')
        ];
        //adatbázisba beírás így sokkal könnyeb ha strukturáljuk az adatszerkezetünket
        if($action == 'new') {
            $qry = "INSERT INTO $dbTable(`" . implode('`,`', array_keys($data)) . "`) VALUES('" . implode("','", $data) . "')";
        }else{
            $qry = "UPDATE $dbTable SET 
            `username` = '{$data['username']}'
            ,`email` = '{$data['email']}'
            ".($data['password']=='' ? "" : ",`password` = '{$data['password']}'").//ha volt kitöltött pass akkor ez az elem az elhashelt jelszót kell tartalmazza már
            " WHERE id = $t_id
            LIMIT 1";
        }

        mysqli_query($link, $qry) or die(mysqli_error($link));
        $az = mysqli_insert_id($link);//sikeres beírás azonosítója
        header('location:'.$baseUrl.'&amp;act=list');
        exit();//átirányítás listázásra

    }
}

//switch a műveletek leválasztására
switch ($action) {
    case 'delete':
        if ($t_id) {
            mysqli_query($link, "DELETE FROM users WHERE id =  '$t_id' LIMIT 1") or die(mysqli_error($link));//törlés
        }
        header('location:?act=list');
        exit();//átirányítás listázásra
        break;

    case 'update':
        //lekérjük az adatokat
        if ($t_id) {
            $qry = "SELECT id,email,username,status FROM users WHERE id = $t_id LIMIT 1";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));//törlés
            $row = mysqli_fetch_assoc($result);
            //var_dump($row);
        }
        //echo 'módosítunk: ' . $t_id;
        //break;
    case 'new':
        //ha van row akkor update van és adatokkal kell feltölteni az űrlapot kiinduláskor, ha nincs row, akkor legyen egy üres tömb ;)
        if(!isset($row)) $row = [];
        $form = '<div class="register-box-body">
    <p class="login-box-msg">Új felhasználó felvitel</p>

    <form method="post">
      <div class="form-group has-feedback">
        <input value="'.checkValue('username',$row).'" name="username" type="text" class="form-control" placeholder="Felhasználónév" >
        <span class="glyphicon glyphicon-user form-control-feedback"></span>'
            . (isset($hiba['username']) ? $hiba['username'] : '') //hiba befűzése a html-kódba ha van
            . '
      </div>
      <div class="form-group has-feedback">
        <input value="'.checkValue('email',$row).'" name="email" type="text" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>'
            . (isset($hiba['email']) ? $hiba['email'] : '') //hiba befűzése a html-kódba ha van
            . '</div>
      <div class="form-group has-feedback">
        <input name="password" type="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>'
            . (isset($hiba['password']) ? $hiba['password'] : '') //hiba befűzése a html-kódba ha van
            . '</div>
      <div class="form-group has-feedback">
        <input name="repassword" type="password" class="form-control" placeholder="Retype password">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
         
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Regisztrálás</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>';
        $moduleOutput = $form;//indexbe előkészítve
        break;
    default:
        $qry = "SELECT * FROM users";
        $results = mysqli_query($link, $qry) or die(mysqli_error($link));
        //tábla fejléc és nyitás
        $table = '<div class="box">
    <!-- /.box-header -->
    <div class="box-body">
        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
            <div class="row">
                <div class="col-sm-6"></div>
                <div class="col-sm-6"></div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <a href="'.$baseUrl.'&amp;act=new" class="btn btn-large btn-primary">új felvitel</a><br><br>
                    <table id="example2" class="table table-bordered table-hover dataTable" role="grid"
                           aria-describedby="example2_info">
                        <thead><tr>
                            <th rowspan="1" colspan="1">id</th>
                            <th rowspan="1" colspan="1">email</th>
                            <th rowspan="1" colspan="1">name</th>
                            <th rowspan="1" colspan="1">status</th>
                            <th rowspan="1" colspan="1">művelet</th>
                        </tr>';
        while ($row = mysqli_fetch_assoc($results)) {
            $table .= '<tr role="row" class="odd">
                            <td class="">' . $row['id'] . '</td>
                            <td>' . $row['email'] . '</td>
                            <td class="sorting_1">' . $row['username'] . '</td>
                            <td>' . $row['status'] . '</td>
                            <td><a href="'.$baseUrl.'&amp;act=update&amp;tid=' . $row['id'] . '">módosít</a> | <a href="'.$baseUrl.'&amp;act=delete&amp;tid=' . $row['id'] . '">törlés</a></td>
                        </tr>';
        }
        //html lezárása a táblának
        $table .= '</thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th rowspan="1" colspan="1">id</th>
                            <th rowspan="1" colspan="1">email</th>
                            <th rowspan="1" colspan="1">name</th>
                            <th rowspan="1" colspan="1">status</th>
                            <th rowspan="1" colspan="1">művelet</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
</div>';

        $moduleOutput = $table;
        break;
}
//kiírás az indexben történik