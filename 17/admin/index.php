<?php
require('../config/connect.php');//db kapcsolat ($link) , így a lejjeb includeolt fileok látni fognak db kapcsolatot
require('../config/functions.php');//saját eljárások betöltése
require('../config/config.php');//közös erőforrások
session_start();//mf indítása
//resources
$o = filter_input(INPUT_GET, 'p') ?: 0;//vagy jön url ből vagy 0 azaz dashboard
$baseUrl = '?p='.$o;//ez kell a modulok linkjeibe
//var_dump($_SESSION,session_id());
//kiléptetünk ha kell
if(filter_input(INPUT_GET,'logout') !== null){
    logout();
}
//ez a file csak jogosultság ellenőrzés után látható, ha nincs akkor ->login
$auth = auth();
if (!$auth) {
    header('location:login.php');
    exit();
}
//module kimenet alapbeállítása
$moduleOutput = '';//ez kell hogy a modulból jöjjön -> doc
//modul betöltése a ../config/config.php alapján, ha létezik a modul
if (array_key_exists($o, $navigation)) {
    $moduleFile = $moduleDir . $navigation[$o]['module'] . $moduleExt;
    if (file_exists($moduleFile)) {
        include($moduleFile);
    } else {
        $moduleOutput = 'Nincs modul: '.$moduleFile;
    }
}
?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ruander - PHP tanfolyam</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <?php
    include 'includes/header.inc';//userbar and header
    include 'includes/navigation.inc';//navigation
    ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Felhasználó adminisztráció
                <small>... alcím</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Felhasználó adminisztráció</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Felhasználók</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                                title="Collapse">
                            <i class="fa fa-minus"></i></button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                                title="Remove">
                            <i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <?php
                    echo $moduleOutput;//modulból jön
                    ?>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    Lábléc / lapozó
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <?php
    include 'includes/footer.inc'; //footer elemek
    ?>


</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="js/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="js/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="js/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!--<script src="../../dist/js/demo.js"></script>-->
<script>
    $(document).ready(function () {
        $('.sidebar-menu').tree()
    })
</script>
</body>
</html>
<?php

mysqli_close($link);//db link lezárása

