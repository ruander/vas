<?php
//közös erőforrások
$secret_key = "S3cr3T_K3y!@";//átlatalános titkosító kulcs
//rendszer konfig
$moduleDir =  'modules/';
$moduleExt = '.php';
//navigáció elemei (később db!)
$navigation = [
    0 => [
        'title' => 'dashboard',
        'icon' => 'dashboard',
        'module'=> 'dashboard'
    ],
    1 => [
        'title' => 'Felhasználók',
        'icon' => 'user',
        'module'=> 'users'
    ],
    2 => [
        'title' => 'Kuponjaim',
        'icon' => 'help',
        'module'=> 'coupons'
    ],
];