<?php
//űrlapfeldolgozás / hibakezelés

if(!empty($_POST)){
    //hibakezelés/szűrés a post elemeire
    //var_dump($_POST);
    $hiba=[];//üres hibatömb
    //név nem lehet üres
    $name = filter_input(INPUT_POST,'name');
    if($name == ""){
        $hiba['name'] = '<span class="error">név mező nem lehet üres!</span>';
    }
    //email ellenőrzése
    $email = filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL);
    if(!$email){
        $hiba['email'] = '<span class="error">Nem érvényes formátum!</span>';
    }

    if(empty($hiba)){//nem került elem a hiba tömbbe azaz minden adat OK!
        $data = [
          'email' => $email,
          'name' => $name,
        ];

        echo var_export($data,true);//olyan mint a var_dump, de csak 1 változót tud kiírni, és ha 2. paraméter true akkor viszatér stringként a kiírandó elemekkel
        exit();//állj
    }
}

?><!doctype html>
<html>
<head>
    <title>Űrlapok</title>
    <meta charset="utf-8">
</head>
<body>
<form method="post">
    <label>
        Karaktered neve<sup>*</sup>: <input type="text" name="name" placeholder="Nameless One" autocomplete="off">
    <?php //ha van hiba írjuk ki, ha van hiba tömbbünk
    if(isset($hiba['name'])){//ha létezik ez az elem, akkor volt hibakezelés és került is bele hiba erre az elemre vonatkozóan
        echo $hiba['name'];
    }
     ?></label>
    <br>
    <label>
        Email címed<sup>*</sup>: <input type="text" name="email" placeholder="email@cim.hu" value="<?php
        //postból visszaírjük a valuet a felhasználói élmény javítása érdekében
        echo filter_input(INPUT_POST,'email');
        ?>">
<?php echo isset($hiba['email']) ? $hiba['email']:'' //short if ?> </label>
    <br><button>Karakter generálás -></button>
</form>
</body>
</html>
