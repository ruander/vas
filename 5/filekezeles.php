<?php
echo '<h2>Mappa kezelés</h2>';
//mappa létezésének ellenőrzése is_dir(dir)
$mappa = 'proba/';
if(is_dir($mappa)){
    echo 'létezik';
}else{
    echo 'nyista';
}
//mappa létrehozása, ha nem létezik
//mkdir($mappa);//ha már létezik, warning! ezért csak akkor futtatjuk ha nem létezik ˇˇˇˇ
if(is_dir($mappa)){
    echo 'létezik';
}else{
    mkdir($mappa);
}
//alkalmazása:
$dir = 'karakterek/';
if(!is_dir($dir)){
    mkdir($dir);
}
//itt már biztosan létezik a karakterek mappa
echo '<h2>File kezelés</h2>';

//file létezésének vizsgálata
$fileName = 'teszt.txt';
if(is_file($fileName)){//itt (./)nem létezik mert másik (proba) mappában van
    echo 'file létezik!';
}
if(is_file($mappa.$fileName)){//már jó az elérési út is, ezért létezik
    echo 'file létezik!';
}
var_dump(file_exists('proba/teszt2'));//nem különböztet meg file-t vagy mappát, ha van true val tér vissza akár file, akár mappa
//file törlése - unlink(filename)
var_dump(unlink($mappa.$fileName));//ha nem létezik warning-ot ad ezért csak ellenörzöt file-t törölj
//file létrehozása (csak ellenőrzött mappába!)
$content = 'Hello World!!!';//ez lesz a file tartalma
//file neve legyen elso.txt
$fileName = 'elso.txt';//override (redeclare)
//proba mappa jo lesz az a $mappa -ban van
file_put_contents($mappa.$fileName,$content);
//file vagy url tartalmának beolvasása
$fileTartalma = file_get_contents($mappa.$fileName);
var_dump($fileTartalma);
//tömb eltárolása file-ba 1.
$tomb=['Joe, a béka','Philippe, az egér'];
var_dump($tomb);
//tömb értékeinek kiírása ha a tömb max 1 dimenzio - implode(ragacs,tomb)
$tombString = implode(',',$tomb);
//kiirjuk fileba
//visszaolvassuk a kiírt file tartalmát
//az implode ellentétje -> explode(ragacs,tomb)
$ujraTomb = explode(',',$tombString);
var_dump($ujraTomb);
//implode -explode fileban tárolásra nem igazán alkalmas
//helyes modszer 1:
$tombSorozat= serialize($tomb);
echo '<br>'.$tombSorozat;
//kiirjuk fileba
//visszaolvassuk a kiírt file tartalmát
var_dump(unserialize($tombSorozat));
//helyes modszer 2.
$tombJSON = json_encode($tomb);//string
//kiirjuk fileba
//visszaolvassuk a kiírt file tartalmát
$tombJSONbol = json_decode($tombJSON);//ujra tomb
var_dump($tombJSON, $tombJSONbol);
//lehet még xml, csv ...
echo '<h1>'.rand(1,12).'</h1>';