<!doctype html>
<html lang="hu">
 <head>
  <title>PHP tanfolyam | Ruander Oktatóközpont</title>
  <meta charset="utf-8">
 </head>
 <body>
 <h2>A PHP és HTML együttműködése</h2>
 <?php
 //php kód helye - egysoros komment : //
 /*
 több
 soros
 komment
 */
 print 'teszt';// operátor:  '' -> string
 echo "<br>teszt2";// operátor:  "" -> string
 echo '<h1>Helo World!</h1>';//a html a php számára csak egyszerű string
 ?>
 <footer>
 Copyright &copy; <?php echo date('Y-m-d H:i:s');//dátum kiírása másodrperc pontossággal betöltődéskor ?>
 </footer>
 </body>
</html>