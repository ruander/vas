<?php
/**
 * Created by PhpStorm.
 * User: hgy
 * Date: 2019. 05. 05.
 * Time: 13:36
 */
//tömb kialakítása a menüpontok adatainak (később ez jöhet config fileból, vagy adatbázisból is)
$menu = [
    1 => [
        'title' => 'kezdőlap',
        'href' => 'kezdolap.html',
        'fa-icon' => 'home',
    ],
    2 => [
        'title' => 'rólunk',
        'href' => 'rolunk.html',
        'fa-icon' => 'user',
    ],
    3 => [
        'title' => 'kapcsolat',
        'href' => 'kapcsolat.html',
        'fa-icon' => 'email',
    ],
];
$navigation = '<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">MyBrand</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">';//menu html elemek nyitásának vége
//menütömb bejárása a menü felépítésére
foreach ($menu as $menuId => $menuElement) {
    $navigation .= '<a class="nav-item nav-link" href="#">' . $menuElement['title'] . '</a>';//aktuális menü elem címe
}

$navigation .= '</div>
    </div>
</nav>';//menu html elemek zárása

echo $navigation;//menü kiírása