<?php
//ha van feldolgozandó adat akkor a szuperglobális tömbjeink tartalmazzák
//var_dump($_GET);//url paraméterekből!
//var_dump($_POST);//file headjében 'utazik'
//var_dump($_REQUEST);
//ha van mit feldolgozni, dolgozzuk fel
if(!empty($_POST)){
    //hibakezelés/szűrés a post elemeire
    var_dump($_POST);
    $hiba=[];//üres hibatömb
    //név nem lehet üres
    $name = filter_input(INPUT_POST,'name');
    if($name == ""){
        $hiba['name'] = 'név mező nem lehet üres!';
    }
    var_dump($name);
    if(empty($hiba)){
        //nem került elem a hiba tömbbe azaz minden adat OK!
        exit('ok!');
    }
}
?><!doctype html>
<html>
<head>
    <title>Űrlapok</title>
    <meta charset="utf-8">
</head>
<body>
<form method="post">
    <label>
        Név: <input type="text" name="name" placeholder="John Doe" autocomplete="off">
    </label>
    <br>
    <label>
        Email: <input type="text" name="email" placeholder="email@cim.hu">
    </label>
    <br><input type="submit" name="submit" value="gyííí">
</form>
</body>
</html>
